package repositories.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import collection.sync.SyncManager;
import collection.tick.TickManager;
import config.ConfigEnvironment;
import repositories.data.bank.AccountDTO;
import repositories.data.pw.PasswordData;
import repositories.data.pw.PasswordDataSet;
import repositories.data.user.UserDTO;
import repositoryServer.data.Repository;
import repositoryServer.server.RepoServer;

public class Repositories {
	
	private static final long WAIT_UNTIL_SAVE = Integer.parseInt(ConfigEnvironment.getProperty("Repository.Files.WaitUntilSave"));

	public static void main(String[] args) {
		new Repositories((args.length >= 1) ? "true".equalsIgnoreCase(args[0]) : false);
	}

	public Repositories(boolean printData) {
		RepoServer repoServer = new RepoServer("Repository.User-PW.Server.Port", "Repository.User-PW.Server.KeyStorePath", "Repository.User-PW.Server.KeyStorePassword");
		Repository accountRepo = repoServer.registerRepo(AccountDTO.class);
		Repository userRepo = repoServer.registerRepo(UserDTO.class);
		Repository entryDataRepo = repoServer.registerRepo(PasswordData.class);
		Repository entryDataSetRepo = repoServer.registerRepo(PasswordDataSet.class);

		if (printData) {
			try {
				File f = new File("./csv/");
				f.mkdirs();
				Files.write(Paths.get("./csv/pwData.csv"), entryDataRepo.toString().getBytes());
				Files.write(Paths.get("./csv/pwDataSet.csv"), entryDataSetRepo.toString().getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		new TickManager(() -> repoServer.tick(), 1);

		new Thread(new Runnable() {
			SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (true) {
					try {
						accountRepo.save();
						userRepo.save();
						entryDataRepo.save();
						entryDataSetRepo.save();
					} catch (IOException e) {
						e.printStackTrace();
					}
					syncManager.asyncronizedWait(WAIT_UNTIL_SAVE);
				}
			}
		}).start();
	}

}
