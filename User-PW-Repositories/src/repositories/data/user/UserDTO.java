package repositories.data.user;

import java.io.Serializable;
import java.util.UUID;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.UUIDGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repositories.User")
@DatabaseEntity(name = "User")
public class UserDTO implements DTO, Serializable {

	private static final long serialVersionUID = 365099233617162107L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_UUID, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = UUIDGenerator.class)
	private UUID id;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	@DatabaseEntry(type = EntryType.DATA, contentType = EntityContentType.UNIQUE)
	private String name = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	@DatabaseEntry(type = EntryType.DATA, contentType = EntityContentType.STANDART)
	private String password = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	@DatabaseEntry(type = EntryType.DATA, contentType = EntityContentType.UNIQUE)
	private String eMail = "";

	public UserDTO() {
		super();
	}

	public UserDTO(UUID id, String name, String password, String eMail) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.eMail = eMail;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEMail() {
		return eMail;
	}

	@Override
	public DTO copy() {
		return new UserDTO(id, name, password, eMail);
	}

	@Override
	public String toString() {
		return "UserDB [id=" + id + ", name=" + name + ", password=" + password + ", eMail=" + eMail + "]";
	}
}
