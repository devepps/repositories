package repositories.data.bank;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.LongGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repositories.Account")
@DatabaseEntity(name = "Account")
public class AccountDTO implements DTO, Serializable {

	private static final long serialVersionUID = 7801065928531326689L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = LongGenerator.class)
	private Long userId = 0l;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	@DatabaseEntry(type = EntryType.DATA, contentType = EntityContentType.STANDART)
	private Long money = 0l;

	public AccountDTO() {
		super();
	}

	public AccountDTO(Long userId, Long money) {
		super();
		this.userId = userId;
		this.money = money;
	}

	public Long getUserId() {
		return userId;
	}

	public Long getMoney() {
		return money;
	}

	public void setMoney(Long money) {
		this.money = money;
	}

	@Override
	public DTO copy() {
		return new AccountDTO(userId, money);
	}

	@Override
	public String toString() {
		return "AccountDTO [userId=" + userId + ", money=" + money + "]";
	}

}
