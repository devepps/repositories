package repositories.data.pw;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.MultiDataReaderType;
import baseSC.data.types.SingleDataReaderType;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.IntegerGenerator;
import repository.data.generator.LocalDateTimeNowGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repositories.PWDataSet")
@DatabaseEntity(name = "PasswordDataSet")
public class PasswordDataSet implements DTO, Serializable {

	private static final long serialVersionUID = 7944297233135306922L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = IntegerGenerator.class)
	private Integer id = 0;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_UUID, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private UUID userId;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	@DatabaseEntry(type = EntryType.DATA)
	private String name;

	@PackagableContent(dataTypeKey = MultiDataReaderType.KEY_ARRAY_LIST, contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private ArrayList<Integer> dataIds = new ArrayList<>();

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_DATE_TIME, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA, generator = LocalDateTimeNowGenerator.class, contentType = EntityContentType.GENERATED)
	private LocalDateTime created = LocalDateTime.now();

	public PasswordDataSet() {
		super();
	}

	public PasswordDataSet(int id, UUID userId, String name) {
		this.id = id;
		this.userId = userId;
		this.name = name;
	}

	public PasswordDataSet(int id, UUID userId, String name, LocalDateTime created) {
		this.id = id;
		this.userId = userId;
		this.name = name;
		this.created = created;
	}

	public int getId() {
		return id;
	}

	public UUID getUserId() {
		return userId;
	}

	public ArrayList<Integer> getDataIds() {
		return dataIds;
	}

	public String getName() {
		return name;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	@Override
	public String toString() {
		return "PasswordDataSet [id=" + id + ", userId=" + userId + ", name=" + name + ", dataIds=" + dataIds
				+ ", created=" + created + "]";
	}

	@SuppressWarnings("unchecked")
	@Override
	public DTO copy() {
		PasswordDataSet dataSet = new PasswordDataSet(id, userId, name, created);
		dataSet.dataIds = (ArrayList<Integer>) this.dataIds.clone();
		return dataSet;
	}

}
