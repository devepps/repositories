package repositories.data.pw;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.IntegerGenerator;
import repository.data.generator.LocalDateTimeNowGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repositories.PWData")
@DatabaseEntity(name = "PasswordData")
public class PasswordData implements DTO, Serializable {

	private static final long serialVersionUID = 697828110430768779L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = IntegerGenerator.class)
	private Integer id = 0;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_BYTE_ARRAY, defaultValueId = DefaultValue.ID_EMPTY_BYTE_ARRAY)
	@DatabaseEntry(type = EntryType.DATA)
	private byte[] key = new byte[0];

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_BYTE_ARRAY, defaultValueId = DefaultValue.ID_EMPTY_BYTE_ARRAY)
	@DatabaseEntry(type = EntryType.DATA)
	private byte[] value = new byte[0];

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_BOOLEAN, defaultValueId = DefaultValue.ID_BOOLEAN_FALSE)
	@DatabaseEntry(type = EntryType.DATA)
	private boolean isSecured = false;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_DATE_TIME, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA, generator = LocalDateTimeNowGenerator.class, contentType = EntityContentType.GENERATED)
	private LocalDateTime created = LocalDateTime.now();

	public PasswordData() {
		super();
	}

	public PasswordData(Integer id, byte[] key, byte[] value, boolean isSecured, LocalDateTime created) {
		this.id = id;
		this.key = key;
		this.value = value;
		this.isSecured = isSecured;
		this.created = created;
	}

	public PasswordData(Integer id, byte[] key, byte[] value) {
		this.id = id;
		this.key = key;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public byte[] getKey() {
		return key;
	}

	public byte[] getValue() {
		return value;
	}

	public boolean isSecured() {
		return isSecured;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	@Override
	public String toString() {
		return "PasswordData [id=" + id + ", key=" + Arrays.toString(key) + ", value=" + Arrays.toString(value)
				+ ", isSecured=" + isSecured + ", created=" + created + "]";
	}

	@Override
	public DTO copy() {
		return new PasswordData(id, Arrays.copyOf(key, key.length), Arrays.copyOf(value, value.length), isSecured, created);
	}

}
